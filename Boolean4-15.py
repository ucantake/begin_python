import time, math

strat_time = time.time()
array_int=[]
"""
проверка на противоположность и однородность 
(-5 и 5 противоложные)
определение возрастающей или убывающей последовательности
поиск чисел которые одинаково читаются с разных сторон 
"""
g, i, p, c = 1, 1, 0, 0
quantity = int(input("введите количество чисел "))

for i in range(quantity):
    a = int(input("введите число "))

    if a > 0:
        print("число положительное ")
        p += 1
        number_size = int(math.log10(a)) + 1
    else:
        number_size = int(math.log10(-a)) + 2
        print("число отрицательное")
    if a % 2 == 0:
        print("число четное")
        c += 1
    else:
        print("число нечетное")
    array_int.append(a)  # добавление элемента в массив

print("в последовательности ", c, "четных и ", p, "положительных")
print(time.time() - strat_time)
# ошибка с f
