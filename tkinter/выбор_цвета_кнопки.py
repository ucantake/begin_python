from tkinter import *#подключение библиотеки 
 
def change():#новая функция, которая будет снимать значения кнопок
    if var.get() == 0:#var.get() необходим для снятия значения переменной 
        label['bg'] = 'red'# присвоение методу label параметр bg (цвет) атрибута red
    elif var.get() == 1:
        label['bg'] = 'green'
    elif var.get() == 2:
        label['bg'] = 'blue'
 
root = Tk()
 
var = IntVar()#IntVar необходим для возможности снятия значений с кнопок
var.set(0)# установит значение по умолчанию в переменной var равной нулю
red = Radiobutton(text="Red", variable=var, value=0)
green = Radiobutton(text="Green", variable=var, value=1)
blue = Radiobutton(text="Blue", variable=var, value=2)
button = Button(text="Изменить", command=change)# при нажатии на кнопку будет вызываться функция, которая будет проверять состояние кнопки и меняться цвет
label = Label(width=20, height=10)
red.pack()# размещение в окне элемента
green.pack()
blue.pack()
button.pack()
label.pack()
 
root.mainloop()# вызов окна root 