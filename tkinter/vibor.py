#!окно с выбором цветов

#импорт бибилиотеки для python3
from tkinter import * #

#объявление открытия окна
root = Tk() #присваивание к переменной root модуля (бибилиотеки) tkinter

#блок параметров окна
root.geometry('350x400+400+300')#задание размеров окна
root.resizable(width=False,height=False)#параметр запрещающий изменение размера окна

lab=Label(root,text="Сколько штук?",font="Arial 12")
lab.pack()


var = IntVar()#IntVar необходим для возможности снятия значений с кнопок
var.set(-1)#устанавливает значение по умолчанию для переменных принимающих с радиокнопок

rad0 = Radiobutton(root,text="0-11",variable=var,value=0)#создает радиокнопку и присваивает ее к переменной
rad0.pack()#устанавливает элемент в окне

rad1 = Radiobutton(root,text="11-20",variable=var,value=1)
rad1.pack()

rad3 = Radiobutton(root,text="21-30",variable=var,value=2)
rad3.pack()

rad4 = Radiobutton(root,text="31-40",variable=var,value=3)
rad4.pack()

lab1=Label(root,text="Какого цвета?",font="Arial 12")
lab1.pack()


# для каждого флажка нужна своя переменная
c1 = IntVar()#IntVar необходим для возможности снятия значений с кнопок
c2 = IntVar()#IntVar необходим для возможности снятия значений с кнопок
c3 = IntVar()#IntVar необходим для возможности снятия значений с кнопок
c4 = IntVar()#IntVar необходим для возможности снятия значений с кнопок

che1= Checkbutton(root,text="RED",variable=c1,onvalue=1,offvalue=0,bg="red")#устанавливает флажки и присваивает его к переменной
che1.pack()

che2= Checkbutton(root,text="BLUE",variable=c2,onvalue=2,offvalue=0,bg="blue")
che2.pack()

che3= Checkbutton(root,text="GREEN",variable=c3,onvalue=3,offvalue=0,bg="green")
che3.pack()

che4= Checkbutton(root,text="YELLOW",variable=c4,onvalue=4,offvalue=0,bg="yellow")
che4.pack()

root.mainloop()# вызов окна root